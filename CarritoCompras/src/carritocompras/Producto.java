package carritocompras;

public class Producto {
    
    //Atributos
    private String nombreProducto;
    private int numProducto;
    private int cantidadDisp;
    private int precioKg;

    // Constructores
    public Producto() {
    }

    public Producto(String nombreProducto, int numProducto, int cantidadDisp, int precioKg) {
        this.nombreProducto = nombreProducto;
        this.numProducto = numProducto;
        this.cantidadDisp = cantidadDisp;
        this.precioKg = precioKg;
    }

    // Get y set
    public String getNombreProducto() {
        return nombreProducto;
    }

    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    public int getNumProducto() {
        return numProducto;
    }

    public void setNumProducto(int numProducto) {
        this.numProducto = numProducto;
    }

    public int getCantidadDisp() {
        return cantidadDisp;
    }

    public void setCantidadDisp(int cantidadDisp) {
        this.cantidadDisp = cantidadDisp;
    }

    public int getPrecioKg() {
        return precioKg;
    }

    public void setPrecioKg(int precioKg) {
        this.precioKg = precioKg;
    }
    
    // ToString para imprimir lista de productos
    @Override
    public String toString() {
        return String.format("%-28s", nombreProducto) + String.format("%-22s", numProducto) 
                + String.format("%-22s", cantidadDisp) + ("$" + precioKg);
    }
    
}