package carritocompras;

import java.util.Scanner;

public class CarritoCompras {
    
    // Imprime el mensaje y salta una línea
    static void imprimirMsjL(String sMsj){
        System.out.println(sMsj);
    }
    
    // Imprime el mensaje sin saltar una línea
    static void imprimirMsjSinL(String sMsj){
        System.out.print(sMsj);
    }
    
    // Imprime un separador
    static void separador(){
        imprimirMsjL("-----------------------------------------------------------------------------------");
    }
    // Imprime información del programa y personal
    static void infoInicial(){        
        separador();
        imprimirMsjL("                         Universidad Autónoma de Campeche");
        imprimirMsjL("                             Facultad de Ingeniería");
        imprimirMsjL("                      Ingeniería en Sistemas Computacionales");
        imprimirMsjL("                          Lenguaje de Programación I, 2A");        
        imprimirMsjL("                          Proyecto 1: Carrito de Compras\n");
        imprimirMsjL("Integrantes del equipo:");
        imprimirMsjL("57039.- Chin Pech Jessica J. Estefanía.");
        imprimirMsjL("56618.- Colli Pech Leslie A.");
        imprimirMsjL("57569.- Pérez Pérez Karen E.");
        imprimirMsjL("56964.- Rodriguez Cab Omar J.");
        imprimirMsjL("57618.- Uc Vazquez D. Carolina.");
        separador();
    }
    
    // Imprime información final
    static void infoFinal(){
        imprimirMsjL("-UAC-FDI-ISC-LDP1-2A-CARRITODECOMPRAS-");
        separador();
    }
    
    // Menú con opciones posibles a realizar
    static void menu(){
        Scanner tec = new Scanner (System.in);
        imprimirMsjL("                               = CARRITO DE COMPRAS =");
        imprimirMsjL("MENÚ.");
        imprimirMsjL("Opción 1.- Ingresar.");
        imprimirMsjL("Opción 2.- Salir.");
        imprimirMsjSinL("Ingrese el número de opción elegido: ");
        int iOpcion = tec.nextInt();
        separador();
        opMenu(iOpcion);
    }
    
    // Realiza la opción del menú deseada
    static void opMenu(int iOpcion){
        switch (iOpcion){
            case 1:
                imprimirMsjL("INGRESAR.");
                Usuarios();
                break;
            case 2:
                imprimirMsjL("¡Vuelva pronto!");
                separador();
                break;
            default: 
                imprimirMsjL("Opción inexistente. Intente nuevamente.");
                separador ();
                menu();             
        }
    }
    
    // Usuarios y arreglo
    static void Usuarios(){         
        Usuario usuario1 = new Usuario("Edgar", "uac", 1500, "plus", 15);
        Usuario usuario2 = new Usuario("Jessica", "estefania28", 1000, "frecuente", 10);       
        Usuario usuario3 = new Usuario("Leslie", "abc", 1000, "frecuente", 10);
        Usuario usuario4 = new Usuario("Karen", "password", 1000, "frecuente", 10);       
        Usuario usuario5 = new Usuario("Omar", "slipknot", 505, "ocasional", 5);      
        Usuario usuario6 = new Usuario("Carolina", "junio", 1000, "frecuente", 10);
        Usuario[] usuarioValidable = {usuario1, usuario2, usuario3, usuario4, usuario5, usuario6};
        validarUsuario(usuarioValidable);
    }

    // Valida el usuario
    static void validarUsuario(Usuario[] usuarioValidable){
        Scanner tec = new Scanner (System.in);
        imprimirMsjSinL("Usuario: ");
        String sUsuario = tec.nextLine();
        imprimirMsjSinL("Contraseña: ");
        String sContraseña = tec.nextLine();
        int iNoValidado = 0;        
        for (Usuario usuarioValidado:usuarioValidable){
            if(sUsuario.equals(usuarioValidado.getNombre()) && 
                    sContraseña.equals(usuarioValidado.getPassword())){
                separador();
                System.out.println("Has ingresado correctamente al sistema, " + usuarioValidado.getNombre() + ".");
                imprimirMsjL("Bienvenid@ a tu Carrito de Compras.");
                separador();
                Productos(usuarioValidado);
            }
            else{
                iNoValidado++;
            } 
        }
        if (iNoValidado == 6){
            separador();
            imprimirMsjL("Usuario incorrecto o contraseña incorrecta.");
            imprimirMsjL("Se le dirigirá al menú principal.");
            separador();
            menu();
        }
    }
    
    // Productos y arreglo
    static void Productos (Usuario usuarioValidado){
        Producto producto1 = new Producto("Carne de res", 101, 20, 40);
        Producto producto2 = new Producto("Pescado", 102, 20, 35);
        Producto producto3 = new Producto("Pechuga de pollo", 103, 30, 25);
        Producto producto4 = new Producto("Huevo", 104, 30, 30);
        Producto producto5 = new Producto("Manzana", 105, 20, 30);
        Producto producto6 = new Producto("Plátano", 106, 20, 15);
        Producto producto7 = new Producto("Pera", 107, 20, 40);
        Producto producto8 = new Producto("Espagueti", 108, 10, 5);
        Producto producto9 = new Producto("Plumilla", 109, 5, 3);
        Producto producto10 = new Producto("Coditos", 110, 10, 5);
        Producto producto11 = new Producto("Leche", 111, 40, 20);
        Producto producto12 = new Producto("Yogurt", 112, 30, 30);
        Producto producto13 = new Producto("Helado", 113, 20, 40);
        Producto producto14 = new Producto("Cerveza", 114, 50, 100);
        Producto producto15 = new Producto("Vino", 115, 15, 150);
        Producto producto16 = new Producto("Vodka", 116, 20, 100);
        Producto producto17 = new Producto("Pinol", 117, 15, 40);
        Producto producto18 = new Producto("Cloro", 118, 15, 30);
        Producto producto19 = new Producto("Ace", 119, 15, 35);
        Producto producto20 = new Producto("Budín", 120, 10, 10);
        Producto producto21 = new Producto("Hojaldra", 121, 10, 15);
        Producto producto22 = new Producto("Dona", 122, 10, 7);
        Producto productoComprable [] = {producto1, producto2, producto3, producto4, producto5, producto6, 
            producto7, producto8, producto9, producto10, producto11, producto12, producto13, producto14, 
            producto15, producto16, producto17, producto18, producto19, producto20, producto21, producto22};
        listaProductos(usuarioValidado, productoComprable);
    }
    
    // Imprime lista de productos
    static void listaProductos(Usuario usuarioValidado, Producto productoComprable []){
        imprimirMsjL("                               - LISTA DE PRODUCTOS -");
        System.out.println(String.format("%-18s", "     Producto") + String.format("%-23s", "Número de Producto") +
                String.format("%-24s", "Cantidad Disponible") +  "Precio por kg (l)") ;
        for (Producto productoComprar:productoComprable){
            System.out.println(productoComprar.toString());
        }
        separador();
        submenu(usuarioValidado, productoComprable);
    }

    // Submenú para comprar
    static void submenu(Usuario usuarioValidado, Producto productoComprable []){
        Scanner tec = new Scanner (System.in);
        imprimirMsjL("SUBMENÚ.");
        imprimirMsjL("1.- Comprar.");
        imprimirMsjL("2.- Cerrar sesión.");
        imprimirMsjSinL("Ingresa el número de opción elegido: ");
        int iOpcion2 = tec.nextInt();
        separador();
        opSubmenu(usuarioValidado, productoComprable, iOpcion2);
    }
    
    // Realiza la opción del submenú deseada
    static void opSubmenu(Usuario usuarioValidado, Producto productoComprable [], int iOpcion2){
        switch (iOpcion2){
            case 1:
                imprimirMsjL("COMPRAR.");
                comprarProducto(usuarioValidado, productoComprable);
                break;
            case 2:
                imprimirMsjL("¡Hasta luego, " + usuarioValidado.getNombre() + "!");                
                separador();
                menu();
                break;
            default: 
                imprimirMsjL("Opción inexistente. Intente nuevamente.");
                separador ();
                submenu(usuarioValidado, productoComprable);
        }
    }  
    
    // Comprar
    static void comprarProducto(Usuario usuarioValidado, Producto productoComprable []){
        Scanner tec = new Scanner (System.in);
        int iTipoProductosAdq = 0;
        int iTotalKgAdq = 0;
        double iCostoProductos = 0;
        int iAgregarProducto = 0;
        int iNoComprado = 0;
        
        do{
            imprimirMsjSinL("Ingrese el número del producto que desea agregar al carrito: ");
            int iNumProducto = tec.nextInt();
            for (Producto productoComprado:productoComprable){
                if (iNumProducto == productoComprado.getNumProducto()) {
                    imprimirMsjSinL("Ingresa el número de kilogramos (litros) que deseas de " +
                            productoComprado.getNombreProducto() + " (" + productoComprado.getNumProducto() + "): ");
                    int iNumKg = tec.nextInt();
                    separador();
                    System.out.println("→ Has añadido " + iNumKg + " kilogramos (litros) del producto " +
                            productoComprado.getNombreProducto() + "(" + productoComprado.getNumProducto() + ") a tu carrito.");                 
                    System.out.println("Precio unitario: $" + productoComprado.getPrecioKg() +
                            "Importe: $" + (iNumKg * productoComprado.getPrecioKg()));
                    separador();
                    iTipoProductosAdq++;
                    iTotalKgAdq = iTotalKgAdq + iNumKg;
                    iCostoProductos = iCostoProductos + (iNumKg * productoComprado.getPrecioKg());
                }
                else {
                    iNoComprado++;                    
                }
            
            }
            if (iNoComprado == 22){
            separador();
            imprimirMsjL("Número de producto inexistente.");
            separador();
            }
            imprimirMsjL("¿Deseas continuar comprando?");
            imprimirMsjL("- Sí, deseo agregar otro producto (número 1).");
            imprimirMsjL("- No, deseo finalizar mi compra (número cualquiera).");
            imprimirMsjSinL("Ingresa el número de opción elegido: ");            
            int iOpcion3 = tec.nextInt();
            separador();
            if (iOpcion3 != 1){
                iAgregarProducto++;
            }
        } while (iAgregarProducto == 0);
        
        if (iTipoProductosAdq > 0){
            reciboCompra(usuarioValidado, productoComprable, iTipoProductosAdq, iTotalKgAdq, iCostoProductos);
        }
        else {
            imprimirMsjL("No has agregado productos a tu carrito de compras.");
            imprimirMsjL("Se te dirigirá al submenú.");
            separador();
            submenu(usuarioValidado, productoComprable);
        }
        
    }
    
    // Recibo de compra
    static void reciboCompra(Usuario usuarioValidado, Producto productoComprable[], 
                int iTipoProductosAdq, int iTotalKgAdq, double iCostoProductos){
        double dSaldoInicial = usuarioValidado.getSaldoTarjeta();
        double iCostoInicial = iCostoProductos;
        double dDescuento = ((iCostoProductos * usuarioValidado.getDescuento())/100);
        iCostoProductos = iCostoProductos - dDescuento;
        System.out.println(usuarioValidado.toString());
        System.out.println("Debido a que eres cliente " + usuarioValidado.getCliente() + 
                ", se te aplicará un descuento del " + usuarioValidado.getDescuento() + "%.");
        System.out.println("Subtotal (costo inicial de los productos): $" + iCostoInicial);
        System.out.println("Total (costo final con descuento aplicado): $" + iCostoProductos);
        separador();
        if (iCostoProductos < usuarioValidado.getSaldoTarjeta()){
            usuarioValidado.disminuirSaldoT(iCostoProductos);
            imprimirMsjL("¡Enhorabuena, " + usuarioValidado.getNombre() + ", tu compra fue realizada con éxito!");
            separador();
            imprimirMsjL("RECIBO DE COMPRA.");
            System.out.println("Saldo inicial: $" + dSaldoInicial);
            System.out.println("Total de tipos de productos adquiridos: " + iTipoProductosAdq);
            System.out.println("Total de kilogramos (litros) adquiridos: " + iTotalKgAdq);
            System.out.println("Subtotal(costo inicial de los productos): $" + iCostoInicial);
            System.out.println("Descuento (" + usuarioValidado.getDescuento() + "%) :" + dDescuento);
            System.out.println("Total (costo final con descuento aplicado): $" + iCostoProductos);
            System.out.println("Saldo final: " + usuarioValidado.getSaldoTarjeta());
            separador();
            System.out.println("¡Gracias por tu compra, " + usuarioValidado.getNombre() + "!");
            imprimirMsjL("Se te dirigirá al submenú.");            
            separador();
            submenu(usuarioValidado, productoComprable);
        } else {
            imprimirMsjL("No se puede realizar esta compra ya que no cuentas con el saldo suficiente."); 
            imprimirMsjL("Se te dirigirá al submenú.");
            separador();
            submenu(usuarioValidado, productoComprable);
        }       
    }
    
    // Run
    public static void main(String[] args) {
        infoInicial();
        menu();
        infoFinal();
    }    
}