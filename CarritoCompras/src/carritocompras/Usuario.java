package carritocompras;

public class Usuario {
    
    // Atributos
    private String nombre;
    private String password;
    private double saldoTarjeta;
    private String cliente;
    private double descuento;

    public Usuario() {
    }

    public Usuario(String nombre, String password, double saldoTarjeta, String cliente, double descuento) {
        this.nombre = nombre;
        this.password = password;
        this.saldoTarjeta = saldoTarjeta;
        this.cliente = cliente;
        this.descuento = descuento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public double getSaldoTarjeta() {
        return saldoTarjeta;
    }

    public void setSaldoTarjeta(double saldoTarjeta) {
        this.saldoTarjeta = saldoTarjeta;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public double getDescuento() {
        return descuento;
    }

    public void setDescuento(double descuento) {
        this.descuento = descuento;
    }
    
    public void disminuirSaldoT(double valor){
        this.saldoTarjeta = this.saldoTarjeta - valor;
    }
        
    @Override // No password, ¿quitar? 
    public String toString() {
        return "Hola, " + nombre + ", tu saldo actual es de $" + saldoTarjeta;
    }    
    
}
